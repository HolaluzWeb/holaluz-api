
## Installation

- Add the package to your `composer.json` file and run `composer update`:
```json
{
    "require": {
        "holaluz/api": "0.1.*"
    }
}
```

If using Laravel framework:

- Add the service provider to your `config/app.php` file, inside the `providers` array: `'Holaluz\Api\ApiServiceProvider'`
- Add the service provider to your `config/app.php` file, inside the `providers` array: `'HolaluzApi' => Holaluz\Api\Facades\HolaluzApi::class`
- Publish the config file by running the following command in the terminal: `php artisan vendor:publish`
- Edit the config files: `config/holaluzapi.php` for Holaluz Api client specific options.
