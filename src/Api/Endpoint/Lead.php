<?php

namespace Holaluz\Api\Endpoint;

class Lead extends AbstractApi
{
    private $key;
    private $value;

    public function create(Array $data)
    {
        return $this->client->request("lead/create", $data);
    }

    public function find($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
        return $this;
    }

    public function update($data)
    {
        $key = $this->key;
        $value = $this->value;
        return $this->client->request("lead/update/$key/$value", $data);
    }

    /** Adds infusionsoft tag to the contact **/
    public function tag($tagId)
    {
        $key = $this->key;
        $value = $this->value;
        return $this->client->request("lead/tag/$key/$value/$tagId");
    }

}