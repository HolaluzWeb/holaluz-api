<?php

namespace Holaluz\Api\Endpoint;

use Holaluz\Api\Client;

abstract class AbstractApi {

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

}