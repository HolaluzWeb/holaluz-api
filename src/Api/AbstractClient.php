<?php

namespace Holaluz\Api;

use Guzzle\Http\Client as GuzzleClient;

/** Simplifies making Guzzle calls to laravel app API endpoints */
abstract class AbstractClient
{
    private $apikey;
    private $apiUrl;

    private $endpoints;

    public function __construct(array $config)
    {
        $this->apikey = $config['apikey'];
        $this->apiUrl = $config['url'] . "/";
        $this->endpoints = [];
    }

    //@todo I don't like this method is public
    public function request($uri, Array $data = [])
    {
        $client = new GuzzleClient();
        $client->setDefaultOption('exceptions', false);
        
        $request = $client->post($this->apiUrl . $uri, [
                'apikey' => $this->apikey, // TODO from config
                'Accept' => 'application/json'

        ], $data);

        return new Response($request->send());
    }

    /**
     * Returns the requested class name, optionally using a cached array so no
     * object is instantiated more than once during a request.
     *
     * @param string $class
     * @return mixed
     */
    public function getEndpoint($class)
    {
        $class = '\Holaluz\Api\Endpoint\\' . $class;

        if (!array_key_exists($class, $this->endpoints)) {
            $this->endpoints[$class] = new $class($this);
        }

        return $this->endpoints[$class];
    }


}
