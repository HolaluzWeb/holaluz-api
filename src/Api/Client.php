<?php

namespace Holaluz\Api;

use Guzzle\Http\Client as GuzzleClient;

/** Simplifies making Guzzle calls to laravel app API endpoints */
class Client extends AbstractClient
{
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /** Set all endpoints here **/
    public function leads()
    {
        return $this->getEndpoint('Lead');
    }

}
