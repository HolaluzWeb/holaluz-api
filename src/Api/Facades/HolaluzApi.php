<?php

namespace Holaluz\Api\Facades;

use Illuminate\Support\Facades\Facade;

class HolaluzApi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'holaluzapi';
    }
}