<?php    
namespace Holaluz\Api;

class Response
{
    private $response;

    public function __construct($response)
    {
    	$this->response = $response;
    }

    public function getStatus()
    {
    	return $this->response->getStatusCode();
    }

    public function isSuccess()
    {
    	return ($this->getStatus() == 200);
    }

    public function getResponse()
    {
    	return $this->response->getBody()->__toString();
    }
    
}